package com.cungnh.gallery;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cungnh.gallery.photoentity.PhotoEntity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_READ_STORAGE = 101;
    private List<String> mListPath;
    private List<PhotoEntity> listData;
    private RecyclerView rvImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }

        initData();

        rvImage = findViewById(R.id.rv_image);
        rvImage.setLayoutManager(new GridLayoutManager(this, 3));
        initRvImage();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_STORAGE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    private void initData() {
        final String column = "_data";
        final String[] projection = {column};
        mListPath = new ArrayList<>();

        try (Cursor cursor = getApplicationContext().getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    String path = cursor.getString(index);
                    if (path != null && !path.isEmpty()) {
                        mListPath.add(path);
                    }
                    cursor.moveToNext();
                }
            }
        }

    }

    //    public String getRealPathFromURI(Context context, Uri contentUri) {
//        Cursor cursor = null;
//        try {
//            String[] proj = { MediaStore.Images.Media.DATA };
//            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            return cursor.getString(column_index);
//        } finally {
//            if (cursor != null) {
//                cursor.close();
//            }
//        }
//
    private void initRvImage() {
        listData = new ArrayList<>();
        for (int i = 0; i < mListPath.size(); i++) {
            listData.add(new PhotoEntity(mListPath.get(i)));
        }

        PhotoAdapter photoAdapter = new PhotoAdapter(listData, this);

        Toast.makeText(this, mListPath.get(0).toString() + "", Toast.LENGTH_SHORT).show();
        rvImage.setAdapter(photoAdapter);
    }
}
