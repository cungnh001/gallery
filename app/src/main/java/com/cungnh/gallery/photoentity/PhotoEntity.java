package com.cungnh.gallery.photoentity;

public class PhotoEntity {
    private String imgPath ;

    public PhotoEntity(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }
}
