package com.cungnh.gallery;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cungnh.gallery.photoentity.PhotoEntity;

import java.util.ArrayList;
import java.util.List;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder> {
    private List<PhotoEntity> listData;
    private Context context;

    public PhotoAdapter(List<PhotoEntity> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @NonNull
    @Override
    public PhotoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false);
        return new PhotoHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull PhotoHolder holder, int position) {
        String data = listData.get(position).getImgPath();

        setAdater();

        Glide.with(context)
                .load(data)
                .into(holder.ivImage);
    }

    private void setAdater() {


    }

    @Override



    public int getItemCount() {
        listData = new ArrayList<>();
        return listData.size();
    }

    static class PhotoHolder extends RecyclerView.ViewHolder {
        String imgPath;
        ImageView ivImage;

        PhotoHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_image);
        }
    }
}
